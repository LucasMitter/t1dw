const logger = require('../../config/logger');

module.exports.errors = (app, req, res, error) => {
    logger.log({
        level: "error",
        message: error.message
    })
    switch(error.code){
        case 'ECONNREFUSED':
            res.render('errors.ejs', {message: "Erro de conexão com o Banco de dados"}) 
            break
        case 'ER_ACCESS_DENIED_ERROR':
            res.render('errors.ejs', {message: "Erro de autenticação com o Banco de dados"}) 
            break
        case 'ER_BAD_DB_ERROR':
            res.render('errors.ejs', {message: "Erro database não encontrada no Banco de dados"}) 
            break
        case 'ECONNRESET':
            res.render('errors.ejs', {message: "Erro Banco de dados desconectado"}) 
            break
        
        default:
            res.render('errors.ejs', {message: `Erro desconhecido: ${error.code}`})    
    }
}

  