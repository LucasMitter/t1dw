const dbConnection = require('../../config/dbConnection');
const logger = require('../../config/logger');
const {getUploads} = require ('../models/upload');
const {errors} = require ('./errors')

module.exports.home = (app, req, res) => {
    params = req.query
    console.log(params)
    console.log('[Controller Home]')
    dbConn = dbConnection()
    getUploads(dbConn, (error, result) =>{
        dbConn.end()
        if(error) {
            errors(app,req,res,error)
        } else res.render('home.ejs', {uploads: result})
    })
}

  