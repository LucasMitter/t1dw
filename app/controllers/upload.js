const dbConnection = require('../../config/dbConnection')
const {check, validationResult} = require('express-validator')
const logger = require('../../config/logger')
const {errors} = require ('./errors')
const multer = require('multer')
const fs  = require('fs')
const {getUpload, getUploads, insertUpload, updateUpload} = require ('../models/upload');


var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        var dir = 'public/uploads';
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        callback(null, dir);
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
});

var multerupload = multer({storage: storage}).array('files', 12);
module.exports.upload = (app, req, res) => {
    dbConn = dbConnection()
    multerupload(req, res, function (err) {
        let uploadObj = req.body
        try{
            uploadObj.url = "/uploads/" + req.files[0].originalname
        }catch(e){
            res.render('result.ejs', {message: "Erro ao fazer upload."})
            return
        }
        if (err) {
            res.render('result.ejs', {message: "Erro ao fazer upload."})
            return
        }        
        insertUpload(uploadObj, dbConn, (error, result) => {
            dbConn.end()
            if(error) errors(app,req,res,error)
            else res.render('result.ejs', {message: "Upload completo."})
        })
    })
}

  