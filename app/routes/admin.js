const {check, validationResult} = require('express-validator')
const {settings} = require('../controllers/settings')
const {upload} = require('../controllers/upload')
const multerupload = require('../../config/multerupload')

module.exports = {
    admin: (app) => {
        app.get('/settings', function (req, res) {
            settings(app, req, res)
        })
        app.post('/settings',
        [
            check('login').isLength({min:3,max: 128}).withMessage('Login deve conter entre 3 e 128 caracteres'),
            check('senha').isLength({min:3, max:128}).withMessage('Digite deve conter entre 3 e 128 caracteres')
        ], function (req, res) {
            let errors = validationResult(req)
            if (!errors.isEmpty()) {
                errors = errors.array()
                res.render('login', {error: errors, login: req.body})
                return
            }
            settings(app, req, res)
        })
        app.get('/uploadform', function (req, res) {
            // upload(app, req, res)
            res.render('uploadform.ejs', {err:{}, uploadObj:{}})
        })
        app.post('/upload',
            (req, res) => {
                upload(app, req, res) //Novo controller
            }
        )
    }
    
}
