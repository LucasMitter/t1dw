const { home } = require('../controllers/home.js')
const { login } = require('../controllers/login.js')


module.exports = {
    routes: (app) => {
        app.get('/', function(req, res){
            home(app, req, res) // controller da home
        })
        app.get('/login', function(req, res){
            login(app, req, res) // controller da home
        })
    }
}


