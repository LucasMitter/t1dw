const multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        var dir = 'public/uploads';
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        callback(null, dir);
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
});

var multerupload = multer({storage: storage}).array('files', 12)
module.exports = multerupload