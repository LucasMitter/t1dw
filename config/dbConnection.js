const mysql = require('mysql');

const host = 'localhost'; 
const database = 'trabalho1'; 
const user = 'root'; 
const password = '';

module.exports = () =>{
    return dbConn = mysql.createConnection({ 
        host: host, 
        user: user, 
        password: password, 
        database: database
    });
}
