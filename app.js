const app = require('./config/server.js')

const routes = require('./app/routes/routes.js')
routes.routes(app)


const admin = require('./app/routes/admin.js')
admin.admin(app)
